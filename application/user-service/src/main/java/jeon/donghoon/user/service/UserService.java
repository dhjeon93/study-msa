package jeon.donghoon.user.service;

import jeon.donghoon.user.dto.UserDto;

public interface UserService {

    UserDto createUser(UserDto userDto);

}

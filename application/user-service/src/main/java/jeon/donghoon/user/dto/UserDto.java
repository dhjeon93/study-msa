package jeon.donghoon.user.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@Getter
@Setter
public class UserDto {

    private String email;
    private String name;
    private String pwd;
    private String userId;
    private ZonedDateTime createdAt;

    private String encryptedPwd;

}

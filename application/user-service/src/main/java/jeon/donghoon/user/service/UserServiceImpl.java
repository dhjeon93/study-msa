package jeon.donghoon.user.service;

import jeon.donghoon.user.dto.UserDto;
import jeon.donghoon.user.jpa.UserEntity;
import jeon.donghoon.user.jpa.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDto createUser(UserDto userDto) {
        userDto.setUserId(UUID.randomUUID().toString());

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserEntity user = mapper.map(userDto, UserEntity.class);
        user.setEncryptedPwd("encrypted");

        user = userRepository.save(user);

        return userDto;
    }
}

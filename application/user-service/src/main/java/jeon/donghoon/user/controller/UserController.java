package jeon.donghoon.user.controller;

import jeon.donghoon.user.dto.UserDto;
import jeon.donghoon.user.service.UserService;
import jeon.donghoon.user.vo.Greeting;
import jeon.donghoon.user.vo.RequestUser;
import jeon.donghoon.user.vo.ResponseUser;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final Greeting greeting;

    @GetMapping("health-check")
    public String status() {
        return "It's working in User Service.";
    }

    @GetMapping("welcome")
    public String welcome() {
        return greeting.getMessage();
    }

    @PostMapping("user")
    public ResponseEntity createUser(@RequestBody RequestUser requestUser) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        UserDto dto = mapper.map(requestUser, UserDto.class);
         dto = userService.createUser(dto);

        ResponseUser response = mapper.map(dto, ResponseUser.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
